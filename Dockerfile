# Use TensorFlow with GPU support as the base image
# TensorFlow 2.7.0 is used to match the version used in SLEAP 1.3.4
FROM tensorflow/tensorflow:2.7.0-gpu

# Import the public key for NVIDIA's repositories
RUN apt-get install -y --no-install-recommends gnupg2 && \
    apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/3bf863cc.pub

# Install build tools, minimal graphics libraries
# https://github.com/conda-forge/opencv-feedstock/issues/401
RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    libgl1-mesa-glx \
    libglapi-mesa \
    libegl-mesa0 \
    libegl1 \
    libopengl0 \
    libfontconfig1 \
    libxkbcommon-x11-0 \
    && rm -rf /var/lib/apt/lists/*

# Stable SLEAP release is installed from PyPI (https://pypi.org/project/sleap/)
RUN pip install --no-cache-dir sleap[pypi]==1.3.4

# Set working directory
WORKDIR /workspace

# Copy application files
COPY ./src /workspace/src

# Set QT debug environment variable to help debug issues with Qt plugins
ENV QT_DEBUG_PLUGINS=1